

function removePossiblyStupidNativeAd(cssSelector) {
  const nodes = document.querySelectorAll(cssSelector);
    if (nodes.length > 0) {
      node = nodes[0];
      console.log("Removing node: " + cssSelector);
      node.parentNode.removeChild(node);
    }
}
function removeStupidNativeAds() {
  removePossiblyStupidNativeAd("ytd-player-legacy-desktop-watch-ads-renderer");
  removePossiblyStupidNativeAd("ytd-promoted-sparkles-web-renderer");
}

// Select the node that will be observed for mutations
const targetNode = document.body;

// Options for the observer (which mutations to observe)
const config = { childList: true, subtree: true };

// Callback function to execute when mutations are observed
const callback = function(mutationList, observer) {
    // Use traditional 'for loops' for IE 11
    for(const mutation of mutationList) {
      removeStupidNativeAds();
    }
};

// Create an observer instance linked to the callback function
const observer = new MutationObserver(callback);

// Start observing the target node for configured mutations
observer.observe(targetNode, config);

 removeStupidNativeAds();

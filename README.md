# borderify

**This add-on removes annoying, (and usually distasteful) native ads from youtube.com

## What it does

This extension just includes:

* a content script, "extension.js", that is injected into any pages
under "youtube.com/" or any of its subdomains

It looks for several dom element names and removes them.

## Subject to change

The dom is not a contract, and will likely be updated, causing this to break either intentionally or unintentionally. 
